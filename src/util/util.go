package util

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"os"
	"fmt"
)

var Db *sql.DB

func init() {
	Db = createDb()
}

func createDb() (db *sql.DB) {
	dbPort:=os.Getenv("DB_PORT")
	dbHost:=os.Getenv("DB_HOST")
	dbName:=os.Getenv("DB_NAME")
	dbUser:=os.Getenv("DB_USER")
	dbPass:=os.Getenv("DB_PASS")

	//company:company@tcp(localhost:3306)/COMPANYDB

	db, err := sql.Open("mysql", makeConnectString(dbUser, dbPass, dbHost, dbPort, dbName))
	if err != nil {
		panic(err)
	}
	err = db.Ping()
	if err = db.Ping(); err != nil {
		panic(err)
	}
	db.SetMaxOpenConns(50)
	return db
}

func makeConnectString(user string, pass string, host string, port string, name string) string{
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8", user, pass, host, port, name)

}