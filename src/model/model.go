package model

import "time"

type Company struct {
	Id      int64  `json:"id,omitempty"`
	Name    string `json:"name"`
	RegCode string `json:"reg_code,omitempty"`
}

type Contract struct {
	Id      int64     `json:"id,omitempty"`
	Seller  int64     `json:"seller"`
	Client  int64     `json:"client"`
	Number  string    `json:"number"`
	Signed  time.Time `json:"signed"`
	ValidTo time.Time `json:"valid"`
	Credits int64     `json:"credits"`
}

type Purchase struct {
	Id           int64     `json:"id,omitempty"`
	ContractId   int64     `json:"contract_id"`
	PurchaseDate time.Time `json:"purchase_date"`
	Credits      int64     `json:"credits"`
}
