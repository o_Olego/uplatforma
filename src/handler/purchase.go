package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"database/sql"
	m "../model"
	u "../util"
)

func CreatePurchase(c *gin.Context) {
	var purchase m.Purchase

	err := c.BindJSON(&purchase)
	checkErr(c,  errMsgCreate, purch, err)

	tx, err := u.Db.Begin()
	checkErr(c, errMsgCreate, purch, err)

	checkPurchase(tx, purchase, c)

	stmt, err := tx.Prepare("INSERT INTO purchase (cid, date, creditcnt) VALUES (?,?,?)")
	checkErrTx(c, tx, errMsgCreate, purch, err)
	defer stmt.Close()

	res, err := stmt.Exec(purchase.ContractId, purchase.PurchaseDate, purchase.Credits)
	checkErrTx(c, tx, errMsgCreate, purch, err)

	affected, _ := res.RowsAffected()
	purchase.Id, _ = res.LastInsertId()
	if affected == 1 {
		tx.Commit()
		c.JSON(http.StatusCreated, gin.H{
			"message":  "Purchase created successfully",
			"purchase": purchase,
			"count":    1,
		})
	}

}

func checkPurchase(tx *sql.Tx, purchase m.Purchase, c *gin.Context) {
	var contract m.Contract
	var currentAmount int64
	row := tx.QueryRow("select cid, creditcnt from contract where cid=? and active='Y' for update", purchase.ContractId)
	err := row.Scan(&contract.Id, &contract.Credits)
	if err != nil {
		tx.Rollback()
		doAbort(c, http.StatusBadRequest, "Contract not found")
	}

	row = tx.QueryRow("select sum(creditcnt) from purchase where cid=?", purchase.ContractId)
	err = row.Scan(&currentAmount)
	checkErrTx(c, tx, errMsgCreate, purch, err)

	if contract.Credits < purchase.Credits+currentAmount {
		tx.Rollback()
		doAbort(c, http.StatusBadRequest, "Purchase too large")
	}
}