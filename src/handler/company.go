package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"fmt"
	m "../model"
	u "../util"
)

func CreateCompany(c *gin.Context) {
	var company m.Company
	err := c.BindJSON(&company)
	checkErr(c, errMsgCreate, comp, err)

	checkCompany(c, company)

	stmt, err := u.Db.Prepare("insert into company (cname, regcode) values (?, ?)")
	checkErr(c, errMsgCreate, comp, err)

	defer stmt.Close()
	res, err := stmt.Exec(company.Name, company.RegCode)
	checkErr(c, errMsgCreate, comp, err)

	affected, _ := res.RowsAffected()
	company.Id, _ = res.LastInsertId()

	if affected == 1 {
		c.JSON(http.StatusCreated, gin.H{
			"message": fmt.Sprintf(okMsgCreate, compBig, company.Name),
			"company": company,
			"count":   1,
		})
	} else {
		doAbort(c, http.StatusInternalServerError, fmt.Sprintf(errMsgCreate, comp, errUnknown))
	}
}

func GetAllCompanies(c *gin.Context) {
	var (
		company   m.Company
		companies []m.Company
	)

	rows, err := u.Db.Query("select cid, cname, regcode from company where active='Y'")
	checkErr(c, errMsgGetAll, comp, err)
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&company.Id, &company.Name, &company.RegCode)
		companies = append(companies, company)
		if err != nil {
			fmt.Println("Parse error" + err.Error()) // todo logger
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"companies": companies,
		"count":     len(companies),
	})
}

func GetCompany(c *gin.Context) {
	var (
		company m.Company
		result  gin.H
	)
	id := getId(c)
	row := u.Db.QueryRow("select cid, cname, regcode from company where active='Y' and cid = ?", id)
	err := row.Scan(&company.Id, &company.Name, &company.RegCode)
	if err != nil {
		result = gin.H{
			"company": nil,
			"count":   0,
		}
	} else {
		result = gin.H{
			"company": company,
			"count":   1,
		}
	}
	c.JSON(http.StatusOK, result)
}

func UpdateCompany(c *gin.Context) {
	var company m.Company

	id := getId(c)

	err := c.BindJSON(&company)
	checkErr(c, errMsgUpdate, comp, err)

	checkCompany(c, company)
	stmt, err := u.Db.Prepare("update company set cname=?, regcode=?  where cid=? and active='Y'")
	checkErr(c, errMsgUpdate, comp, err)
	defer stmt.Close()

	res, err := stmt.Exec(company.Name, company.RegCode, id)
	checkErr(c, errMsgUpdate, comp, err)

	affected, _ := res.RowsAffected()
	if affected == 1 {
		c.JSON(http.StatusOK, "Company updated successfully")
	} else {
		doAbort(c, http.StatusInternalServerError, errCritical)
	}
}

func DeleteCompany(c *gin.Context) {
	var contracts []int64
	var contract int64

	id := getId(c)

	tx, err := u.Db.Begin()
	checkErr(c, errMsgDelete, comp, err)
	row := tx.QueryRow("select cid from company where cid=? for update", id)
	err = row.Scan(nil)
	checkErrTx(c, tx, errMsgDelete, comp, err)

	rows, err := tx.Query("select cid from contracts where seller=? or client=? for update", id, id)
	checkErrTx(c, tx, errMsgDelete, comp, err)

	for rows.Next() {
		err = rows.Scan(&contract)
		contracts = append(contracts, contract)
		if err != nil {
			fmt.Println("Parse error" + err.Error()) // todo logger
		}
	}

	stmtContract, err := tx.Prepare("update contract set active='N' where cid in (?)")
	checkErrTx(c, tx, errMsgDelete, comp, err)
	defer stmtContract.Close()

	stmtCompany, err := tx.Prepare("update company set active='N' where cid=? and active='Y'")
	checkErrTx(c, tx, errMsgDelete, comp, err)

	defer stmtCompany.Close()

	res, err := stmtContract.Exec(contracts)
	checkErrTx(c, tx, errMsgDelete, comp, err)

	res, err = stmtCompany.Exec(id)
	checkErrTx(c, tx, errMsgDelete, comp, err)

	affected, _ := res.RowsAffected()
	if affected == 1 {
		tx.Commit()
		doAbort(c, http.StatusOK, "Company deleted successfully")
	} else {
		tx.Rollback()
		doAbort(c, http.StatusInternalServerError, errCritical)
	}
}

func checkCompany(c *gin.Context, company m.Company) {
	if len(company.Name) == 0 {
		doAbort(c, http.StatusBadRequest, fmt.Sprintf(errMsgCreate, comp, "Company name can't be empty!"))
	}
}