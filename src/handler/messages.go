package handler

var (
	errCritical = "Critical error. Please notify system administrator"
	errUnknown  = "Unknown error. Please try later"

	errMsgGetAll = "Unable to read all %s: %s"

	errMsgCreate = "Unable to register %s: %s"
	okMsgCreate  = "%s %s registered successfully"

	errMsgUpdate = "Unable to update %s: %s"

	errMsgDelete = "Unable to delete %s: %s"

	comp    = "company"
	compBig = "Company"

	contr    = "contract"
	contrBig = "Contract"

	purch = "purchase"
)
