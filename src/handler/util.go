package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"fmt"
	"database/sql"
)

func checkErr(c *gin.Context, msgBase string, msg string, err error,) {
	if err != nil {
		doAbort(c, http.StatusInternalServerError, fmt.Sprintf(msgBase, msg, err.Error()))
	}
}

func checkErrTx(c *gin.Context, tx *sql.Tx, msgBase string, msg string, err error) {
	if err != nil {
		tx.Rollback()
		doAbort(c, http.StatusInternalServerError, fmt.Sprintf(msgBase, msg, err.Error()))
	}
}

func doAbort(context *gin.Context, code int, message string) {
	context.AbortWithStatusJSON(code, gin.H{"message": message})
}

func getId(context *gin.Context) (id string) {
	id= context.Param("id")
	if id == "" || id == "0" {
		doAbort(context, http.StatusBadRequest, "Id must not be empty")
	}
	return
}
