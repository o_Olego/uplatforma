package handler

import (
	"github.com/gin-gonic/gin"
	"fmt"
	"net/http"
	"time"
	"database/sql"
	m "../model"
	u "../util"
)

func CreateContract(c *gin.Context) {
	var contract m.Contract
	err := c.BindJSON(&contract)
	checkErr(c, errMsgCreate, contr, err)

	checkContract(c, &contract)

	stmt, err := u.Db.Prepare("insert into contract (seller, client, cnumber, datesigned, datevalid, creditcnt) values(?,?,?,?,?,?)")
	checkErr(c, errMsgCreate, contr, err)
	defer stmt.Close()

	res, err := stmt.Exec(contract.Seller, contract.Client, contract.Number, contract.Signed, contract.ValidTo, contract.Credits)
	checkErr(c, errMsgCreate, contr, err)
	affected, _ := res.RowsAffected()
	contract.Id, _ = res.LastInsertId()
	if affected == 1 {
		c.JSON(http.StatusCreated, gin.H{
			"message":  fmt.Sprintf(okMsgCreate, contrBig, contract.Number),
			"contract": contract,
			"count":    1,
		})
	} else {
		doAbort(c, http.StatusInternalServerError, fmt.Sprintf(errMsgCreate, contr, errUnknown))
	}
}

func GetAllContracts(c *gin.Context) {
	var (
		contract  m.Contract
		contracts []m.Contract
	)

	rows, err := u.Db.Query(" select cid, seller,client,cnumber,datesigned,datevalid,creditcnt from contract where active='Y'")
	checkErr(c, errMsgGetAll, contr, err)
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&contract.Id, &contract.Seller, &contract.Client, &contract.Number, &contract.Signed, &contract.ValidTo, &contract.Credits)
		contracts = append(contracts, contract)
		if err != nil {
			fmt.Println("Parse error" + err.Error()) // todo logger
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"contracts": contracts,
		"count":     len(contracts),
	})
}

func GetContract(c *gin.Context) {
	var (
		contract m.Contract
		result   gin.H
	)

	id := getId(c)
	row := u.Db.QueryRow("select cid, seller,client,cnumber,datesigned,datevalid,creditcnt from contract where cid = ? and active='Y'", id)
	if err := row.Scan(&contract.Id, &contract.Seller, &contract.Client, &contract.Number, &contract.Signed, &contract.ValidTo, &contract.Credits); err != nil {
		result = gin.H{
			"company": nil,
			"count":   0,
		}
	} else {
		result = gin.H{
			"contract": contract,
			"count":    1,
		}
	}
	c.JSON(http.StatusOK, result)
}

func UpdateContract(c *gin.Context) {
	var contract m.Contract
	id := getId(c)

	err := c.BindJSON(&contract)
	checkErr(c, errMsgUpdate, contr, err)

	tx, err := u.Db.Begin()
	checkErr(c, errMsgUpdate, contr, err)

	checkContract(c, &contract)
	checkContractUpdate(c, &contract, tx)

	stmt, err := u.Db.Prepare("update contract set cnumber=?,datevalid=?, creditcnt=?  where cid=? and active='Y'")
	checkErrTx(c, tx, errMsgUpdate, comp, err)
	defer stmt.Close()

	res, err := stmt.Exec(contract.Number, contract.ValidTo, contract.Credits, id)
	checkErrTx(c, tx, errMsgUpdate, comp, err)

	affected, _ := res.RowsAffected()
	if affected == 1 {
		tx.Commit()
		doAbort(c, http.StatusOK, "Contract updated successfully")
	} else {
		tx.Rollback()
		doAbort(c, http.StatusInternalServerError, errCritical)
	}
}

func DeleteContract(c *gin.Context) {
	id := getId(c)
	tx, err := u.Db.Begin()
	checkErr(c, errMsgDelete, contr, err)

	row := tx.QueryRow("select cid from contract where cid=? for update", id)
	err = row.Scan(nil)
	checkErrTx(c, tx, errMsgDelete, comp, err)

	stmt, err := tx.Prepare("update contract set active='N' where cid=? and active='Y'")
	checkErrTx(c, tx, errMsgDelete, comp, err)

	defer stmt.Close()

	res, err := stmt.Exec(id)
	checkErrTx(c, tx, errMsgDelete, comp, err)

	affected, _ := res.RowsAffected()
	if affected == 1 {
		tx.Commit()
		doAbort(c, http.StatusOK, "Contract deleted successfully")
	} else {
		tx.Rollback()
		doAbort(c, http.StatusInternalServerError, errCritical)
	}
}

func checkContract(c *gin.Context, contract *m.Contract) {
	var isCompaniesCorrect int64

	if contract.Signed.After(contract.ValidTo) {
		doAbort(c, http.StatusBadRequest, fmt.Sprintf(errMsgCreate, contr, "Date till valid must be after date signed!"))
	}

	if contract.Credits <= 0 {
		doAbort(c, http.StatusBadRequest, fmt.Sprintf(errMsgCreate, contr, "Credits must be greater than zero!"))
	}

	row := u.Db.QueryRow("select count(*) from company where cid in (?,?) and status ='Y'", contract.Seller, contract.Client)
	err := row.Scan(isCompaniesCorrect)
	checkErr(c, errMsgCreate, contr, err)

	if isCompaniesCorrect != 2 {
		doAbort(c, http.StatusBadRequest, fmt.Sprintf(errMsgCreate, contr, "One or more companies is not registered!"))
	}
}

func checkContractUpdate(c *gin.Context, new *m.Contract, tx *sql.Tx) {
	var old m.Contract
	var purchaseAmount int64
	row := tx.QueryRow("select cid, seller,client,cnumber,datesigned,datevalid,creditcnt from contract where cid=? and active='Y' for update")
	err := row.Scan(&old.Id, &old.Seller, &old.Client, &old.Number, &old.Signed, &old.ValidTo, &old.Credits)
	checkErrTx(c, tx, errMsgUpdate, comp, err)

	if !new.Signed.Equal(old.Signed) {
		tx.Rollback()
		doAbort(c, http.StatusBadRequest, fmt.Sprintf(errMsgCreate, contr, "Date signed can't be modified!"))
	}

	if old.Seller != new.Seller || old.Client != new.Client {
		tx.Rollback()
		doAbort(c, http.StatusBadRequest, fmt.Sprintf(errMsgCreate, contr, "Seller and client can't be modified!"))
	}

	if new.ValidTo.Before(time.Now()) {
		tx.Rollback()
		doAbort(c, http.StatusBadRequest, fmt.Sprintf(errMsgCreate, contr, "Date till valid must be greater or equal current time!"))
	}

	row = tx.QueryRow("select sum(creditcnt) from purchase where cid=?", new.Id)
	err = row.Scan(&purchaseAmount)
	checkErrTx(c, tx, errMsgDelete, comp, err)

	if purchaseAmount > new.Credits {
		tx.Rollback()
		doAbort(c, http.StatusBadRequest, fmt.Sprintf(errMsgCreate, contr, "Credit issued must be greater or equal current purchases amount"))
	}

}