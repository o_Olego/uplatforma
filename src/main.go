package main

import (
	"github.com/gin-gonic/gin"
	h "./handler"
)

func main() {

	//defer db.Close()

	router := gin.Default()
	v1 := router.Group("/api/v1")
	{
		v1.POST("company/", h.CreateCompany)
		v1.GET("company/", h.GetAllCompanies)
		v1.GET("company/:id", h.GetCompany)
		v1.PUT("company/:id", h.UpdateCompany)
		v1.DELETE("company/:id", h.DeleteCompany)
	}
	{
		v1.POST("contract/", h.CreateContract)
		v1.GET("contract/", h.GetAllContracts)
		v1.GET("contract/:id", h.GetContract)
		v1.PUT("contract/:id", h.UpdateContract)
		v1.DELETE("contract/:id", h.DeleteContract)
	}
	{
		v1.POST("purchase/", h.CreatePurchase)
	}
	router.Run(":8080")
}