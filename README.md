## Quick Run Project
First clone the repo then go to go-mysql-crud folder. After that build your image and run by docker. Make sure you have docker in your machine. 

```
git clone https://o_Olego@bitbucket.org/o_Olego/uplatforma.git

cd uplatforma

docker-compose up
```

You need to manually execute ./sql/init.sql. Sorry for this :(